﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        [HttpPost]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            if (string.IsNullOrEmpty(project.name))
                return BadRequest("Project must have a name!");
            int addedId = _projectService.CreateProject(project);
            return new JsonResult(_projectService.GetProjectById(addedId)) {
                StatusCode = (int)HttpStatusCode.Created
            };
        }

        [HttpPut]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            return NoContent();
        }
    }
}
