﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            try
            {
                return Ok(_taskService.GetTaskById(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskDTO task)
        {
            if (string.IsNullOrEmpty(task.name))
                return BadRequest("Task name is not specified");
            var addedId = _taskService.CreateTask(task);
            return new JsonResult(_taskService.GetTaskById(addedId)) {
                StatusCode = (int)HttpStatusCode.Created
            };
        }

        [HttpPut]
        public IActionResult Update([FromBody] TaskDTO task)
        {
            _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _taskService.DeleteTask(id);
                return NoContent();
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
