﻿using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using BSA.BLL.Services;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : Controller
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet]
        [Route("task1/{userId:int}")]
        public ActionResult<Dictionary<ProjectDTO, int>> SelectTasksNumberInProject(int userId)
        {
            try
            {
                return Ok(_linqService.SelectTasksNumberInProject(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task2/{userId:int}")]
        public ActionResult<ICollection<TaskDTO>> SelectTasks(int userId)
        {
            try
            {
                return Ok(_linqService.SelectTasks(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task3/{userId:int}")]
        public ActionResult<IEnumerable<(int, string)>> SelectTasksCompletedInCurrentYear(int userId)
        {
            try
            {
                return Ok(_linqService.SelectTasksCompletedInCurrentYear(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task4")]
        public ActionResult<IEnumerable<(int, string, List<UserDTO>)>> SelectTeamsWithMembers()
        {
            return Ok(_linqService.SelectTeamsWithMembers());
        }

        [HttpGet]
        [Route("task5")]
        public ActionResult<IEnumerable<(UserDTO, IEnumerable<TaskDTO>)>> SelectUsersWithTasks()
        {
            return Ok(_linqService.SelectUsersWithTasks());
        }

        // GET .../api/linq/selecttask6/3
        [HttpGet]
        [Route("task6/{userId:int}")]
        public ActionResult<(UserDTO, int, TaskDTO, ProjectDTO, int)> SelectUserTasksInfo(int userId)
        {
            try
            {
                return Ok(_linqService.SelectUserTasksInfo(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task7")]
        public ActionResult<IEnumerable<(ProjectDTO, TaskDTO, TaskDTO, int)>> GetProjectInfo()
        {
            return Ok(_linqService.GetProjectInfo());
        }

        [HttpGet]
        [Route("task8/{userId:int}")]
        public ActionResult<IEnumerable<TaskDTO>> GetNotDoneTasks(int userId)
        {
            try
            {
                return Ok(_linqService.SelectUserNotFinishedTasks(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
