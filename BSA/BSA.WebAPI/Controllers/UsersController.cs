﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> Get()
        {
            return Ok(_userService.GetUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        [HttpPost]
        public JsonResult Create([FromBody] UserDTO user)
        {
            int addedId = _userService.CreateUser(user);
            var response = new JsonResult(_userService.GetUserById(addedId))
            {
                StatusCode = (int)HttpStatusCode.Created
            };
            return response; 
        }

        [HttpPut]
        public IActionResult Update([FromBody] UserDTO user)
        {
            _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _userService.DeleteUser(id);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            return NoContent();
        }
    }
}
