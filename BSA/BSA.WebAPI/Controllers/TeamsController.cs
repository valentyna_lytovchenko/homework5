﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> Get()
        {
            return Ok(_teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            try
            {
                return Ok(_teamService.GetTeamById(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] TeamDTO team)
        {
            if(string.IsNullOrEmpty(team.name))
                return BadRequest("Team name is not specified");

            int addedId = _teamService.CreateTeam(team);
            return new JsonResult(_teamService.GetTeamById(addedId)) { 
                StatusCode = (int)HttpStatusCode.Created 
            };
        }

        [HttpPut]
        public IActionResult Update([FromBody] TeamDTO team)
        {
            _teamService.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return NoContent();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
