﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<TeamDTO> GetTeams()
        {
            var teams = _context.Teams;
            return _mapper.Map<IList<TeamDTO>>(teams);
        }

        public TeamDTO GetTeamById(int teamId)
        {
            if (!CheckEntityExists(teamId))
                throw new KeyNotFoundException("Team doesn't exist");
            var team = _context.Teams.Find(teamId);
            return _mapper.Map<TeamDTO>(team);
        }

        public int CreateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            var added = _context.Teams.Add(team);
            _context.SaveChanges();
            return added.Entity.Id;
        }

        public void DeleteTeam(int id)
        {
            var team = _context.Teams.Where(t => t.Id == id).FirstOrDefault();
            _context.Teams.Remove(team);
            _context.SaveChanges();
        }

        public void UpdateTeam(TeamDTO teamDto)
        {
            var teamToUpdate = _context.Teams.Find(teamDto.id);

            teamToUpdate.CreatedAt = teamDto.createdAt;
            teamToUpdate.Name = teamDto.name;

            _context.Teams.Update(teamToUpdate);
            _context.SaveChanges();
        }

        protected override bool CheckEntityExists(int teamId)
        {
            return _context.Teams.Find(teamId) != null;
        }
    }
}
