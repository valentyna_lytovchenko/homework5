﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace BSA.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<TaskDTO> GetTasks()
        {
            var tasks = _context.Tasks;
            return _mapper.Map<IList<TaskDTO>>(tasks);
        }

        public TaskDTO GetTaskById(int taskId)
        {
            if(!CheckEntityExists(taskId))
            {
                throw new KeyNotFoundException("Task doesn't exist");
            }
            var task = _context.Tasks.Find(taskId);
            return _mapper.Map<TaskDTO>(task);
        }

        public int CreateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            task.CreatedAt = DateTime.Now;
            task.Performer = _context.Users.Find(taskDto.performerId);
            task.PerformerId = task.Performer?.Id;
            var added = _context.Tasks.Add(task);
            _context.SaveChanges();
            return added.Entity.Id;
        }

        public void DeleteTask(int taskId)
        {
            if(!CheckEntityExists(taskId))
            {
                throw new KeyNotFoundException("Task doesn't exist");
            }
            var task = _context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            _context.Tasks.Remove(task);
            _context.SaveChanges();
        }

        public void UpdateTask(TaskDTO taskDto)
        {
            var taskToUpdate = _context.Tasks.Find(taskDto.id);
            if (taskToUpdate == null)
            {
                throw new KeyNotFoundException($"Task with id#{taskDto.id} not found");
            }

            taskToUpdate.Title = taskDto.name;
            User performer = null;
            if (taskDto.performerId != null &&
               (performer = _context.Users.Find(taskDto.performerId)) == null
            )
            {
                throw new ArgumentException("Given performer doesn't exist");
            }
            taskToUpdate.Performer = performer;
            taskToUpdate.PerformerId = taskToUpdate.Performer?.Id;
            taskToUpdate.Description = taskDto.description;
            taskToUpdate.State = (int)taskDto.state;
            taskToUpdate.FinishedAt = taskDto.finishedAt;

            _context.Tasks.Update(taskToUpdate);
            _context.SaveChanges();
        }
        protected override bool CheckEntityExists(int taskId)
        {
            return _context.Tasks.Find(taskId) != null;
        }
    }
}
