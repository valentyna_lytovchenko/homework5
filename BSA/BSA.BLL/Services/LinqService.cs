﻿using BSA.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class LinqService
    {
        private ProjectService _projectService;
        private UserService _userService;
        private TaskService _taskService;
        private TeamService _teamService;

        public LinqService(
            ProjectService projectService,
            TeamService teamService,
            TaskService taskService,
            UserService userService) 
        {
            _projectService = projectService;
            _userService = userService;
            _taskService = taskService;
            _teamService = teamService;
        }

        public Dictionary<ProjectDTO, int> SelectTasksNumberInProject(int userId) // select №1
        {
            if (!CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            Dictionary<ProjectDTO, int> selected = (_projectService.GetProjects().GroupJoin(
                                                    _taskService.GetTasks().Where(t => t.performerId == userId),
                                                    proj => proj.id,
                                                    t => t.projectId,
                                                    (proj, tasks) => new KeyValuePair<ProjectDTO, int>(proj, tasks.Count())
                                                 )).Where(x => x.Value > 0).
                                                 ToDictionary(x => x.Key, y => y.Value);
            return selected;
        }

        public ICollection<TaskDTO> SelectTasks(int userId) // select №2
        {
            if (!CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            ICollection<TaskDTO> selected = (from tsk in _taskService.GetTasks()
                                                 where tsk.performerId == userId
                                                 where tsk.name.Length < 45
                                                 select tsk).ToList();

            return selected;
        }

        public IEnumerable<(int, string)> SelectTasksCompletedInCurrentYear(int userId) // select №3
        {
            if (!CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var selected = (from tsk in _taskService.GetTasks()
                            where tsk.performerId == userId
                            where tsk.finishedAt.GetValueOrDefault().Year == DateTime.Now.Year
                            select (
                                tsk.id,
                                tsk.name
                            )).ToList();

            return selected;
        }

        public IEnumerable<(int, string, List<UserDTO>)> SelectTeamsWithMembers() // select №4
        {
            var selected = _teamService.GetTeams().Select(t => new { t.id, t.name }).GroupJoin(
                    _userService.GetUsers(),
                    t => t.id,
                    u => u.teamId,
                    (anonTeam, usersEnumerable) => (
                        anonTeam.id,
                        anonTeam.name,
                        usersEnumerable.OrderByDescending(u => u.registeredAt).ToList())
                ).ToList();

            return selected;
        }

        public IEnumerable<(UserDTO, IEnumerable<TaskDTO>)> SelectUsersWithTasks() // select №5
        {
            var selected = _userService.GetUsers().OrderBy(u => u.firstName).GroupJoin(
                    _taskService.GetTasks().OrderByDescending(t => t.name.Length),
                    u => u.id,
                    t => t.performerId,
                    (u, tasks) => ( u, tasks )
                );
            return selected;
        }

        public (UserDTO, int, TaskDTO, ProjectDTO, int) SelectUserTasksInfo(int userId) // select №6
        {
            if (CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var selected = _userService.GetUsers().Where(u => u.id == userId).GroupJoin(
                    _taskService.GetTasks(),
                    u => u.id,
                    t => t.performerId,
                    (u, tasks) => new
                    {
                        user = u,
                        countNotFinishedTasks = tasks.Count(t => (TaskState)t.state == TaskState.InProgress 
                                                                || (TaskState)t.state == TaskState.Canceled),
                        longestTask = tasks.OrderBy(t => t.createdAt - t.finishedAt).First()
                    }
                ).GroupJoin(
                    _projectService.GetProjects(),
                    prevStruct => prevStruct.user.teamId,
                    proj => proj.teamId,
                    (prevStruct, projects) =>
                    (
                        prevStruct.user,
                        prevStruct.countNotFinishedTasks,
                        prevStruct.longestTask,
                        projects.OrderBy(p => p.createdAt).Last(),
                        _taskService.GetTasks().Where(
                            t => t.projectId == projects.OrderBy(p => p.createdAt).Last().id
                        ).Count()
                    )
            ).FirstOrDefault();
            return selected;
        }

        public IEnumerable<(ProjectDTO, TaskDTO, TaskDTO, int)> GetProjectInfo() // select №7
        {
            var tasks = _taskService.GetTasks();
            var selected = _projectService.GetProjects().Select(
                p => (
                        p,
                        tasks.Where(t => t.projectId == p.id).OrderByDescending(t => t.description).First(),
                        tasks.Where(t => t.projectId == p.id).OrderBy(t => t.name).First(),
                        p.description.Length > 20 || tasks.Where(t => t.projectId == p.id).Count() < 3 ?
                        _userService.GetUsers().Count(u => u.teamId == p.teamId) : -1)
            ).ToList();
            return selected;
        }

        public IEnumerable<TaskDTO> SelectUserNotFinishedTasks(int userId)
        {
            if (!CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var tasks = _taskService.GetTasks();
            var selected = tasks.Where(t => t.performerId == userId && t.state != TaskState.Done).ToList();

            return selected;
        }

        private bool CheckUserExists(int userId)
        {
            return _userService.GetUsers().Where(u => u.id == userId).Count() == 1;
        }
    }
}
