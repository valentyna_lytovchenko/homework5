﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<ProjectDTO> GetProjects()
        {
            var projects = _context.Projects;
            return _mapper.Map<IList<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProjectById(int projectId)
        {
            var project = _context.Projects.Find(projectId);
            return _mapper.Map<ProjectDTO>(project);
        }

        public int CreateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            int addedId = _context.Projects.Add(project).Entity.Id;
            _context.SaveChanges();
            return addedId;
        }

        /// <summary>
        /// Deletes project by its id
        /// </summary>
        /// <param name="projectId"></param>
        /// <exception cref="KeyNotFoundException"></exception>
        public void DeleteProject(int projectId)
        {
            if (!CheckEntityExists(projectId))
                throw new KeyNotFoundException("Project doesn't exists");
            var project = _context.Projects.Find(projectId);
            _context.Projects.Remove(project);
            _context.SaveChanges();
        }

        public void UpdateProject(ProjectDTO projectDto)
        {
            var projectToUpdate = _context.Projects.Find(projectDto.id);

            projectToUpdate.AuthorId = projectDto.authorId;
            projectToUpdate.Author = _context.Users.Find(projectDto.authorId);
            projectToUpdate.Deadline = projectDto.deadline;
            projectToUpdate.Description = projectDto.description;
            projectToUpdate.Name = projectDto.name;
            projectToUpdate.TeamId = projectDto.teamId;
            projectToUpdate.Team = _context.Teams.Find(projectDto.teamId);
            projectToUpdate.Tasks = _context.Tasks.Where(t => t.ProjectId == projectDto.id).ToList();

            _context.Projects.Update(projectToUpdate);
            _context.SaveChanges();
        }

        protected override bool CheckEntityExists(int projectId)
        {
            return _context.Projects.Find(projectId) != null;
        }
    }
}
