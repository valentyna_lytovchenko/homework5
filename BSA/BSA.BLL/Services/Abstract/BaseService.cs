﻿using AutoMapper;
using BSA.DAL.Context;

namespace BSA.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly BSADbContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(IMapper mapper, BSADbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        protected abstract bool CheckEntityExists(int entityId);
    }
}
