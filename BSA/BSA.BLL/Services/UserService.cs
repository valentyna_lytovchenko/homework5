﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using System.Collections.Generic;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using BSA.DAL.Context;
using System.Linq;
using System;

namespace BSA.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<UserDTO> GetUsers()
        {
            var users =  _context.Users;
            return _mapper.Map<IList<UserDTO>>(users);
        }

        public UserDTO GetUserById(int userId)
        {
            if (!CheckEntityExists(userId))
            {
                throw new KeyNotFoundException("User is not registered");
            }

            var user = _context.Users.Find(userId);
            return _mapper.Map<UserDTO>(user);
        }

        public int CreateUser(UserDTO userDto)
        {
            var newUser = _mapper.Map<User>(userDto);
            var added = _context.Users.Add(newUser);
            _context.SaveChanges();
            return added.Entity.Id;
        }

        /// <summary>
        /// Deletes a user from db by its id
        /// </summary>
        /// <param name="userId"></param>
        /// <exception cref="KeyNotFoundException"></exception>
        public void DeleteUser(int userId)
        {
            if (!CheckEntityExists(userId))
            {
                throw new KeyNotFoundException("User is not registered, but user delete is called");
            }

            var user = _context.Users.Find(userId);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public void UpdateUser(UserDTO userDto)
        {            
            if(!CheckEntityExists(userDto.id))
            {
                throw new KeyNotFoundException("User is not registered, but user update is called");
            }

            var userToUpdate = _context.Users.Find(userDto.id);
            userToUpdate.BirthDay = userDto.birthDay;
            userToUpdate.Email = userDto.email;
            userToUpdate.FirstName = userDto.firstName;
            userToUpdate.LastName = userDto.lastName;
            if (_context.Teams.Find(userDto.teamId) == null)
            {
                throw new ArgumentException($"Trying to add user to team with id#{ userDto.teamId }, but no such team ");
            }
            userToUpdate.TeamId = userDto.teamId;

            _context.Users.Update(userToUpdate);
            _context.SaveChanges();
        }

        protected override bool CheckEntityExists(int userId)
        {
            return _context.Users.Find(userId) != null;
        }
    }
}
