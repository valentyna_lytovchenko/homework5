﻿using AutoMapper;
using BSA.Common.DTO;
using BSA.DAL.Entities;

namespace BSA.BLL.MapingProfiles
{
    public sealed class TaskProfile: Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>()
                .ForMember(tskDTO => tskDTO.state, src => src.MapFrom(tsk => tsk.State))
                .ForMember(tskDTO => tskDTO.name, src => src.MapFrom(tsk => tsk.Title));

            CreateMap<TaskDTO, Task>()
                .ForMember(tsk => tsk.State, src => src.MapFrom(tskDTO => tskDTO.state))
                .ForMember(tsk => tsk.Performer, src => src.Ignore())
                .ForMember(tsk => tsk.Title, src => src.MapFrom(tskDTO => tskDTO.name));
        }
    }
}
