﻿using AutoMapper;
using BSA.Common.DTO;
using BSA.DAL.Entities;

namespace BSA.BLL.MapingProfiles
{
    public sealed class ProjectProfile: Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>()
                .ForMember(pr => pr.Author, scr => scr.Ignore())
                .ForMember(pr => pr.Team, scr => scr.Ignore());
        }
    }
}
