﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA.DAL.Migrations
{
    public partial class RenameName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                "Name",
                "Tasks",
                "Title"
            );

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(3420));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(4462));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Title" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(1799), "TASK1" });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Title" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(3159), "TASK2" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(235));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 41, 56, 501, DateTimeKind.Local).AddTicks(1278));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 41, 56, 499, DateTimeKind.Local).AddTicks(3449), new DateTime(2021, 7, 3, 20, 41, 56, 483, DateTimeKind.Local).AddTicks(6374) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 41, 56, 499, DateTimeKind.Local).AddTicks(5747), new DateTime(2021, 7, 3, 20, 41, 56, 499, DateTimeKind.Local).AddTicks(5614) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                "Title",
                "Tasks",
                "Name"
            );

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(5775));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(6870));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(4106), null });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(5474), null });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(2898));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 20, 30, 652, DateTimeKind.Local).AddTicks(3809));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 20, 30, 650, DateTimeKind.Local).AddTicks(7145), new DateTime(2021, 7, 3, 20, 20, 30, 636, DateTimeKind.Local).AddTicks(84) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 20, 30, 650, DateTimeKind.Local).AddTicks(9434), new DateTime(2021, 7, 3, 20, 20, 30, 650, DateTimeKind.Local).AddTicks(9297) });
        }
    }
}
