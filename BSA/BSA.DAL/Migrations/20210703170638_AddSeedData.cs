﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(1532), "team1" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(2468), "team2" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(2001, 2, 6, 22, 0, 0, 0, DateTimeKind.Utc), new DateTime(2021, 7, 3, 20, 6, 37, 977, DateTimeKind.Local).AddTicks(4964), "vallit2001@gmail.com", "Valia", "Lytovchenko", new DateTime(2021, 7, 3, 20, 6, 37, 963, DateTimeKind.Local).AddTicks(4499), 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(1998, 4, 9, 21, 0, 0, 0, DateTimeKind.Utc), new DateTime(2021, 7, 3, 20, 6, 37, 977, DateTimeKind.Local).AddTicks(7244), "ogo@gmail.com", "Ondrey", "Gorobets", new DateTime(2021, 7, 3, 20, 6, 37, 977, DateTimeKind.Local).AddTicks(7138), 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(4448), new DateTime(2021, 8, 31, 21, 0, 0, 0, DateTimeKind.Utc), "desctproj1", "proj1", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 2, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(5572), new DateTime(2021, 10, 14, 21, 0, 0, 0, DateTimeKind.Utc), "desctproj2", "proj2", 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "ProjectId1", "State" },
                values: new object[] { 1, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(2771), "somedescr1", new DateTime(2021, 4, 9, 21, 0, 0, 0, DateTimeKind.Utc), "TASK1", 1, 1, null, 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "ProjectId1", "State" },
                values: new object[] { 2, new DateTime(2021, 7, 3, 20, 6, 37, 979, DateTimeKind.Local).AddTicks(4216), "somedescr2", new DateTime(2021, 6, 20, 21, 0, 0, 0, DateTimeKind.Utc), "TASK2", 2, 2, null, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
