﻿using BSA.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BSA.DAL.Context
{
    public class BSADbContext : DbContext 
    {
        public BSADbContext(DbContextOptions<BSADbContext> options) 
            : base(options) 
        { }

        public virtual DbSet<User> Users { get; private set; }

        public virtual DbSet<Team> Teams { get; private set; }

        public virtual DbSet<Task> Tasks { get; private set; }

        public virtual DbSet<Project> Projects { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            
            modelBuilder.Seed();
        }
    }
}
