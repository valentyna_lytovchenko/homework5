﻿using BSA.BLL.MapingProfiles;
using BSA.BLL.Services;
using BSA.DAL.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.WebAPI.IntegrationTests
{
    public class BSAWebAPIApplicationFactory<TStartup> :
        WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services => {
                var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                    typeof(DbContextOptions<BSADbContext>));
                services.Remove(descriptor);

                services.AddDbContext<BSADbContext>(options =>
                    options.UseInMemoryDatabase("TestDB")
                );
                services.AddControllers();

                services.AddScoped<ProjectService>();
                services.AddScoped<TaskService>();
                services.AddScoped<UserService>();
                services.AddScoped<TeamService>();
                services.AddScoped<LinqService>();

                services.AddAutoMapper(cfg => {
                    cfg.AddProfile<ProjectProfile>();
                    cfg.AddProfile<TaskProfile>();
                    cfg.AddProfile<UserProfile>();
                    cfg.AddProfile<TeamProfile>();
                });
            });
        }
    }
}
