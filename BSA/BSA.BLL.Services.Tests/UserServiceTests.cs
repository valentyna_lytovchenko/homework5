using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using BSA.BLL.Services;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using System.IO;
using System;

namespace BSA.BLL.Tests
{
    [Collection("UnitTests")]
    public class UserServiceTests : TestBase
    {
        readonly UserService fakeUserService;
        public DbSet<User> fakeUsersDbSet;
        public DbSet<Team> fakeTeamsDbSet;

        public UserServiceTests()
        {
            fakeUserService = new UserService(mapper, fakeContext);
            fakeUsersDbSet = A.Fake<DbSet<User>>();
            A.CallTo(() => fakeContext.Users).Returns(fakeUsersDbSet);
            A.CallTo(() => fakeContext.Users.Add(A<User>.Ignored)).Returns(null);
        }

        [Fact]
        public void AddUser_WhenNewUser_ThenAddIsCalled()
        {
            var mockUser = A.Fake<UserDTO>();
            try
            {
                fakeUserService.CreateUser(mockUser);
            } catch { }
            A.CallTo(() => fakeUsersDbSet.Add(A<User>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void AddUser_WhenIdExists_ThenThrowInvalidDataException()
        {
            var user = A.Fake<UserDTO>();
            try
            {
                fakeUserService.CreateUser(user);
            } catch { }

            var newUser = A.Fake<UserDTO>();
            newUser.id = user.id;

            try
            {
                fakeUserService.CreateUser(newUser);
            } catch { }
            A.CallTo(() => fakeUsersDbSet.Add(A<User>.Ignored)).Throws<InvalidDataException>();
        }

        [Fact]
        public void AddUserToExistingTeam_ThenAdded()
        {
            var mockUser = new UserDTO() { id = 1, firstName = "Steven", lastName = "Hawking" };
            var mockTeam = new TeamDTO() { id = 10, name = "MockTeam1" };

            var fakeTeamService = new TeamService(mapper, fakeContext);
            try
            {
                fakeTeamService.CreateTeam(mockTeam);
                fakeUserService.CreateUser(mockUser);
            } catch { }
            mockUser.teamId = 10;
            fakeUserService.UpdateUser(mockUser);
            A.CallTo(() => fakeUsersDbSet.Update(A<User>.Ignored)).MustHaveHappenedOnceExactly();
            RefreshContext();
        }

        [Fact]
        public void AddUserToNonExistingTeam_ThenNoUpdate()
        {
            var mockUser = new UserDTO() { id = 1, firstName = "Steven", lastName = "Hawking", teamId = 3 };

            var userService = new UserService(mapper, context);
            userService.CreateUser(mockUser);
            Assert.Throws<ArgumentException>(() => userService.UpdateUser(mockUser));
        }
    }
}
