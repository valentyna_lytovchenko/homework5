﻿using AutoMapper;
using BSA.BLL.MapingProfiles;
using BSA.BLL.Services.Abstract;
using BSA.DAL.Context;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;

namespace BSA.BLL.Tests
{
    public abstract class TestBase
    {
        protected BSADbContext context;
        protected BSADbContext fakeContext;
        static protected IMapper mapper;

        public TestBase()
        {
            fakeContext = A.Fake<BSADbContext>(x => x.WithArgumentsForConstructor(new[] {
                    new DbContextOptionsBuilder<BSADbContext>()
                    .UseInMemoryDatabase("TestDB").Options 
                })
            );
            context = new BSADbContext(
               new DbContextOptionsBuilder<BSADbContext>()
               .UseInMemoryDatabase("TestDB").Options
            );
            var mapConfig = new MapperConfiguration(cfg => {
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new UserProfile());
                cfg.AddProfile(new TaskProfile());
                cfg.AddProfile(new TeamProfile());
            });
            mapper = new Mapper(mapConfig);
        }

        protected void RefreshContext()
        {
            context.Projects.RemoveRange(context.Projects);
            context.Tasks.RemoveRange(context.Tasks);
            context.Users.RemoveRange(context.Users);
            context.Teams.RemoveRange(context.Teams);
            context.SaveChanges();
        }
    }
}
