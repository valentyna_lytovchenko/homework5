﻿using Xunit;
using BSA.BLL.Services;

namespace BSA.BLL.Tests
{
    [Collection("UnitTests")]
    public class ProjectServiceTests : TestBase
    {
        readonly ProjectService projectService;

        public ProjectServiceTests()
        {
            projectService = new ProjectService(mapper, context);
        }

        [Fact]
        public void GetProjects_WhenProjectsAreEmpty_ThenReturn0()
        {
            RefreshContext();
            Assert.Empty(projectService.GetProjects());
        }
    }
}
